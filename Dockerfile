FROM nextflow/nextflow:24.10.4

COPY on_pipeline_finish.sh /
COPY error_weblog.json /

RUN yum install -y jq findutils && \
    nextflow plugin install nf-wave@1.7.4 && \
    nextflow plugin install nf-amazon@2.9.2 && \
    nextflow plugin install nf-weblog@1.1.2 && \
    nextflow plugin install nf-tower@1.9.3
