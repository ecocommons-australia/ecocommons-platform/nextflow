# Nextflow

This is a custom https://www.nextflow.io/ Docker build that provides a stable image source, and adds additional components that would otherwise be downloaded dynamically.
