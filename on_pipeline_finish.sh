#!/bin/bash
# Post pipeline housekeeping that cannot be done within the pipeline itself.
set -ueo pipefail

[[ -z $1 ]] && printf "Usage: on_pipeline_finish.sh EXIT_STATUS [ENV_FILE] [SLEEP_SEC] \n\n" && exit 1

echo "Nextflow exit status: $1"

# Export auth vars from env file
if [[ -e $2 ]]; then
	export $(cat $2 | grep ^KEYCLOAK | xargs)
fi

# Sleep on finish?
# This is for debugging only, it should not be used in production.
if [[ $3 > 0 ]]; then
    sleep $3
fi

function get_token {
	if [[ ! -z ${KEYCLOAK_SERVER_URL+x} ]]; then
		curl -fsS \
		 -d "client_id=${KEYCLOAK_CLIENT_ID}" \
		 -d "client_secret=${KEYCLOAK_CLIENT_SECRET_KEY}" \
		 -d "grant_type=client_credentials" \
	     -X POST "${KEYCLOAK_SERVER_URL}realms/${KEYCLOAK_REALM}/protocol/openid-connect/token" \
	     | jq -j '.access_token'
	fi
}

# There are several cases where Nextflow will terminate before 
# it can send a weblog to the platform indicating the Job status.
# This ensures the Job status is updated by sending a 'final' weblog.
if [[ $1 != 0 && ! -z ${JOB_WEBLOG_URL+x} && ! -z ${NF_WEBLOG_BASIC_TOKEN+x} ]]; then
	ERROR_WEBLOG_JSON="/error_weblog.json"

    echo "Sending Nextflow weblog (${JOB_WEBLOG_URL})"
	curl -fsS \
	    -H "Content-Type: application/json" \
		-H "Authorization: Basic $(echo -n $NF_WEBLOG_BASIC_TOKEN | base64)}" \
		-d "@${ERROR_WEBLOG_JSON}" \
		-X POST $JOB_WEBLOG_URL
fi;

# Save Nextflow report
if [[ ! -z ${NF_REPORT_JOB_SAVE_URL+x} && -f ${NF_REPORT_FILE_PATH} ]]; then
	BEARER_TOKEN=$(get_token)

	echo "Sending report (${NF_REPORT_JOB_SAVE_URL})"
	curl -fsS \
	    -H "Content-Type: text/plain" \
		-H "Authorization: Bearer ${BEARER_TOKEN}" \
		-d "@${NF_REPORT_FILE_PATH}" \
		-X PUT $NF_REPORT_JOB_SAVE_URL
fi
